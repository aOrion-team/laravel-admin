<?php

namespace Getxe\Admin;

use Getxe\Admin\Repositories\ConfigRepository;
use Getxe\Admin\Repositories\Configs\YamlConfigRepository;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router as LaravelRouter;

use Tymon\JWTAuth\Providers\JWTAuthServiceProvider;
use Tymon\JWTAuth\Facades as JWTFacades;
use Spatie\MediaLibrary\MediaLibraryServiceProvider;

use Tymon\JWTAuth\Middleware\GetUserFromToken;
use Tymon\JWTAuth\Middleware\RefreshToken;

use Getxe\Admin\Http\Middleware;
use Getxe\Admin\Commands\TestCommand;

/**
 * A Laravel 5.5 package boilerplate
 *
 * @author: Rémi Collin (remi@code16.fr)
 */
class AdminServiceProvider extends ServiceProvider {

    /**
     * This will be used to register config & view in
     * your package namespace.
     *
     * @var  string
     */
    protected $packageName = 'admin';

    /**
     * A list of artisan commands for your package
     *
     * @var array
     */
    protected $commands = [
        TestCommand::class
    ];

    /**
     * Bootstrap the application services.
     *
     * @param $router
     *
     * @return void
     */
    public function boot(LaravelRouter $router)
    {
        $this->app->bind(ConfigRepository::class, function ($app) {
            return new YamlConfigRepository();
        });

        // Loading third-party package providers
        $this->app->register(JWTAuthServiceProvider::class);
        $this->app->register(MediaLibraryServiceProvider::class);
        $loader = AliasLoader::getInstance();
        $loader->alias('JWTAuth', JWTFacades\JWTAuth::class);
        $loader->alias('JWTFactory', JWTFacades\JWTFactory::class);

        // Loading route middlewares
        $router->aliasMiddleware('auth.jwt', GetUserFromToken::class);
        $router->aliasMiddleware('jwt.refresh', RefreshToken::class);
        $router->aliasMiddleware('cors', Middleware\Cors::class);


        // Register Views from your package
        $this->loadViewsFrom(__DIR__ . '/views', $this->packageName);
        // Regiter migrations
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        // Register translations
        $this->loadTranslationsFrom(__DIR__ . '/lang', $this->packageName);


        $this->publishRoutes();
        $this->publishOthers();

        if ($this->app->runningInConsole()) {
            $this->commands($this->commands);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/config.php', $this->packageName
        );

    }

    protected function publishRoutes() {

        // admin api routes
        $this->app['router']->group(['prefix' => 'admin-api', 'namespace' => 'Getxe\Admin\Http\Controllers'], function () {
            require __DIR__ . '/routes/api.php';
        });

        // admin web routes
        $this->app['router']->group(['prefix' => 'admin', 'namespace' => 'Getxe\Admin\Http\Controllers\Common'], function () {
            require __DIR__ . '/routes/web.php';
        });


        // public routes
        $this->loadRoutesFrom(__DIR__ . '/routes/routes.php');

        // Publish admin_api routes
        $this->publishes([
            __DIR__ . '/routes/admin_api.php' => base_path('/routes/admin_api.php')
        ], 'g-routes');

        // Load published admin api routes
        if(file_exists(base_path('/routes/admin_api.php'))) {
            $this->app['router']->group(['prefix' => 'admin-api', 'namespace' => 'App\Http\Controllers'], function () {
                require base_path('/routes/admin_api.php');
            });
        }
    }

    protected function publishOthers() {
        $this->publishes([
            __DIR__ . '/lang' => resource_path('lang/getxe/'. $this->packageName),
        ], 'g-lang');

        // Register your asset's publisher
        $this->publishes([
            __DIR__ . '/assets' => public_path('getxe/'.$this->packageName),
        ], 'g-public');

        // Publish your seed's publisher
        $this->publishes([
            __DIR__ . '/database/seeds/' => base_path('/database/seeds')
        ], 'g-seeds');

        // Publish your config
        $this->publishes([
            __DIR__ . '/config/config.php' => config_path($this->packageName.'.php'),
            __DIR__ . '/config/admin' => config_path('/admin'),
        ], 'g-config');

    }

}
