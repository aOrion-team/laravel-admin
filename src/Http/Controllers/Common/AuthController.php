<?php

namespace Getxe\Admin\Http\Controllers;

use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

use Getxe\Admin\Http\Requests\AuthRegisterRequest;

class AuthController extends Controller
{

    public function signup(AuthRegisterRequest $request)
    {
        $user = new User([
           'name'  => $request->input('name'),
           'email' => $request->input('email'),
           'password'  => bcrypt($request->input('password'))
        ]);
        $user->save();
        return response()->json([
            'status' => 'success',
            'data' => $user
        ], 201);
    }

    public function signin(Request $request)
    {

        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = $request->only('email','password');

        if(! $token = JWTAuth::attempt($credentials)){
            return response()->json([
                'invalid_credentials' => "Invalid Credentials."
            ],401);
        }

        $user = JWTAuth::authenticate($token);

        return response()->json([
            'status' => 'success',
            'user' => $user
        ],200)
            ->header('Authorization',$token);

    }

    public function loginOther(Request $request)
    {
        $user = User::find($request->get('id'));
        if ( ! $token = JWTAuth::fromUser($user)) {
            return response([
                'invalid_credentials' => "Invalid Credentials."
            ], 404);
        }
        return response([
            'status' => 'success',
            'user'   => $user
        ])
            ->header('Authorization', $token);
    }

    public function getCurrentUser(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        return response()->json([
            'user'  => $user
        ],200);
    }

    public function logout()
    {
        JWTAuth::invalidate(JWTAuth::getToken());
        return response([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }



    public function refresh()
    {
        return response([
            'status' => 'success'
        ]);
    }

    public function changePassword(Request $request)
    {
        $this->validate($request,[
            'old_password'      => 'required',
            'new_password'      => 'required|min:6|regex:/^.*(?=.*[a-zA-Z])(?=.*\d).*$/',
            'repeat_password'   => 'required|same:new_password'
        ]);

        $user = JWTAuth::parseToken()->authenticate();


        JWTAuth::invalidate(JWTAuth::getToken());

        $credentials = [
            'email' => $user->email,
            'password' => $request->input('old_password')
        ];
        if(! $token = JWTAuth::attempt($credentials)){
            return response()->json([
                'old_password' => [
                        'status' => 'error',
                        'error' => 'invalid.password',
                        'msg' => 'Invalid Old Password.'
                    ]
            ],401);
        }

        $user->password = bcrypt($request->input('new_password'));
        $user->save();
        $newToken = JWTAuth::fromUser($user);

        return response([
            'status' => 'success',
            'msg' => 'Пароль успешно изменен',
        ],200)
            ->header('Authorization', $newToken);
    }

    public function changeEmail(Request $request)
    {
        $this->validate($request,[
            'new_email' => 'required|email|unique:users,email',
        ]);


        $user = JWTAuth::parseToken()->authenticate();
        JWTAuth::invalidate(JWTAuth::getToken());

        $user->email = $request->input('new_email');
        $user->save();

        $newToken = JWTAuth::fromUser($user);

        return response([
            'status' => 'success',
            'msg' => 'Email успешно изменен',
            'user' => $user
        ],200)
            ->header('Authorization', $newToken);
    }
}
