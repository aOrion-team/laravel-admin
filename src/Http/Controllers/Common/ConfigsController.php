<?php

namespace Getxe\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Getxe\Admin\Repositories\ConfigRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class ConfigsController extends Controller {

    protected $repo;

    public function __construct(ConfigRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
   * Display a listing of the resource.
   *
   * @param  String $resource_name
   * @param  String $config_name
   *
   * @return Response
   *
   *
   */
    public function config($resource_name ,$config_name)
    {
        $this->repo->setup($resource_name,$config_name);
        $config = $this->repo->get();
        if(is_null($config)){
            return response()->json(['status' => 'error', 'message' => 'no such config'],404);
        }else{
            return response()->json($config,200);
        }
    }
}
