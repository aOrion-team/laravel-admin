<?php

namespace Getxe\Admin\Http\Controllers;


use Getxe\Admin\Repositories\Resources\BaseFormRepository;

class BaseFormController extends ResourceController
{
    public function __construct(BaseFormRepository $repo)
    {
        parent::__construct($repo);
    }
}
