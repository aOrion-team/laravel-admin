<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 19.11.2017
 * Time: 11:57
 */

namespace Getxe\Admin\Http\Controllers;


use Illuminate\Http\Request;

interface IResourceController
{
    public function index(Request $request);
    public function create();
    public function store(Request $request);
    public function show($id);
    public function edit($id);
    public function update(Request $request, $id);
    public function destroy($id);
}