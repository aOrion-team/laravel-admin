<?php

namespace Getxe\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as NewImage;

use Spatie\Image\Image;

class MediaItemController extends Controller
{

    static $disk = 'media_library';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = Storage::disk(self::$disk)->files();
        $dirs = Storage::disk(self::$disk)->directories();
        return Response(['files'=>$files, 'dirs'=>$dirs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $disk = self::$disk;
        $folder = 'default';
        $file = $request->file;
//        $path = $file->store($folder, $disk);
        //TODO: Убрать расширение из слага ато получается jpg.jpg
        $originalExt = $file->getClientOriginalExtension();
        $originalNameSlug = str_slug(basename($file->getClientOriginalName(), '.' . $originalExt));
        $path = Storage::url($disk . '/' . $file->storeAs(
                $folder, $originalNameSlug . '.' . $originalExt, $disk
            ));
        return Response([
            'created' => [
                'url' => url($path),
                'path' => $path,
                'name' => $originalNameSlug . '.' . $originalExt,
                'size' => $file->getClientSize(),
            ]
        ])->header('Content-Type', 'application/json');
    }

    public function crop(Request $request)
    {
        $disk = self::$disk;
        $folder = 'default';
        $image = $request->input('image');
        $cropSize = $request->input('crop_size');
        $path = $image['path'];

        $full_path = public_path().$path;

        $image = NewImage::make($full_path);

        $image_info = pathinfo($full_path);
        $arrFileName = [
            $image_info['filename'],
            floor($cropSize['width']),
            floor($cropSize['height'])
        ];
        $resized_image_name = join("_",$arrFileName).'.'.$image_info['extension'];

        $folder = '/storage/media_library/resize/';
        $path = $folder.$resized_image_name;

        File::exists(public_path().$folder) or File::makeDirectory(public_path().$folder);
        $image->crop(floor($cropSize['width']), floor($cropSize['height']), (int) $cropSize['x'], (int) $cropSize['y'])
            ->save(public_path().$path);

        return Response([
            'created'=>[
                'path'  =>  $path,
                'url'   =>  url($path)
            ]
        ],200)->header('Content-Type', 'application/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
