<?php

namespace Getxe\Admin\Http\Controllers;

use App\Http\Controllers\Controller;

use Getxe\Admin\Repositories\Configs\YamlConfigRepository;
use Illuminate\Http\Request;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Cache;

abstract class PageController extends Controller
{

    use PageTrait;
    use StaticTrait;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeStaticModel($this->app);
        $this->makePageModel($this->app);

    }

    protected $app;

    /**
     * Page name
     * @var
     * */
    protected $page_name;
    /**
     * Url of page
     * @var
     * */
    protected $url_name;
    /**
     * Don't forget about postfix in config file name
     * @var
     * */
    protected $pageNamePostfix = '';
    /**
     * Languages of page
     * @var
     * */
    protected $languages = [];

    public function index(Request $request)
    {
        //fetch SitePages and add to $settings
        $pages = collect();
        if($this->page_model_class() !== false) {
            $pages = $this->getPages($this->url_name);
        }
        $result = $pages->union($this->getStatic($pages->keys()->toArray(), 'custom-pages',$this->url_name.$this->pageNamePostfix)
        )->all();
        return response()->json($result,200);
    }

    public function store(Request $request)
    {
        $page_keys = collect();
        if($this->page_model_class() !== false) {
            $page_keys = $this->savePages($this->url_name, $request->all(), 'url_name', $this->page_name);
        }
        $except = $page_keys->all();
        $configRepo = new YamlConfigRepository('custom-pages', $this->url_name.$this->pageNamePostfix);
        $config = $configRepo->get();
        $data = $request->only(collect($config['fields'])
            ->collapse()
            ->except($except)
            ->keys()
            ->toArray());

        $data = $this->saveStatic($data);

        Cache::forget($this->url_name.$this->pageNamePostfix);
        return response()->json([
            'set' => $data,
            'status'=>'ok'
        ],200);
    }
}
