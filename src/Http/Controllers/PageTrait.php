<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 23.11.2017
 * Time: 22:30
 */

namespace Getxe\Admin\Http\Controllers;

use Illuminate\Container\Container as App;
use Getxe\Admin\Models\PagesModel;
use Getxe\Admin\Repositories\Pages\PagesModelRepository;

trait PageTrait
{
    protected $page_model;

    abstract protected function page_model_class();

    protected function makePageModel(App $app) {

        if($this->page_model_class() !== false) {
            $model = $app->make($this->page_model_class());

            if (!$model instanceof PagesModel)
                throw new \Exception("Class {$this->page_model_class()} must be an instance of Getxe\\Admin\\Models\\PagesModel");

            return $this->page_model = $model;
        } else {
            return false;
        }

    }

    protected function getPages($id, $attribute = 'url_name', $relations = false) {
        $settings = collect([ $attribute => $id ]);
        $pageRepo = new PagesModelRepository($this->page_model);
        $pageRepo->withRelations($relations);
        if(count($this->languages) > 0) {
            foreach ($this->languages as $lang) {
                $settings = $settings->merge($pageRepo->get($id, $attribute, $lang));
            }
        } else {
            $settings = $settings->merge($pageRepo->get($id, $attribute));
        }
        return $settings;
    }

    protected function savePages($id, $data, $attribute = 'url_name', $page_name = null) {
        $page_keys = collect();
        $pageRepo = new PagesModelRepository($this->page_model);
        if(count($this->languages) > 0) {
            foreach ($this->languages as $lang) {
                $page_keys = $page_keys->merge($pageRepo->save($id, collect($data), $attribute, $page_name, $lang));
            }
        } else {
            $page_keys = $page_keys->merge($pageRepo->save($id, collect($data), $attribute, $page_name));
        }
        return $page_keys;
    }

    protected function createPages($data, $page_name = null) {
        $page_keys = collect();
        $id = null;
        $pageRepo = new PagesModelRepository($this->page_model);
        if(count($this->languages) > 0) {
            foreach ($this->languages as $lang) {
                $created = $pageRepo->create(collect($data), $page_name, $lang);
                $id = $created['id'];
                $page_keys = $page_keys->merge($created['page_keys']);
            }
        } else {
            $created = $pageRepo->create(collect($data), $page_name);
            $id = $created['id'];
            $page_keys = $page_keys->merge($created['page_keys']);
        }
        return [ 'id' => $id, 'page_keys' => $page_keys ];
    }

}