<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 23.11.2017
 * Time: 22:51
 */

namespace Getxe\Admin\Http\Controllers\Pages;


use Getxe\Admin\Http\Controllers\ResourceController;
use Getxe\Admin\Repositories\Pages\ResourcePageRepository;
use Illuminate\Http\Request;


abstract class LpController extends ResourceController
{

    protected $resource_name;

    protected $languages = [];

    public function __construct(ResourcePageRepository $repo)
    {
        $repo->setLanguages($this->languages);
        $repo->setResourceName($this->resource_name);
        parent::__construct($repo);
    }

    public function store(Request $request)
    {
        $item = $this->repo->create($request->all());
        return response()->json(['data'=>$item, 'status'=>'ok'],200);
    }

    public function update(Request $request, $id)
    {
        $updated = $this->repo->update($request->all(), $id);
        return response()->json([
            'data'=> $this->repo->find($id),
            'updated' => $updated,
            'status'=> ($updated) ? 'ok' : 'error'
        ],200);
    }

    public function create()
    {
        return  response()->json($this->repo->find(null),200);
    }
}