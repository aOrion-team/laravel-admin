<?php

namespace Getxe\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use Getxe\Admin\Models\Settings\CommonSetting;

class SettingsPageController extends PageController
{
    protected $page_name = 'Общие настройки';
    protected $url_name = 'common-settings';
    protected $languages = [];
    protected $pageNamePostfix = '';

    protected function static_model_class()
    {
        return CommonSetting::class;
    }

    protected function page_model_class()
    {
        return false;
    }
}
