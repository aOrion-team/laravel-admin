<?php

namespace Getxe\Admin\Http\Controllers;

use App\Http\Controllers\Controller;

use Getxe\Admin\Repositories\Criterias\ExcludeIdsCriteria;
use Getxe\Admin\Repositories\Criterias\FilterCriteria;
use Getxe\Admin\Repositories\Criterias\SearchByFieldsCriteria;
use Getxe\Admin\Repositories\Criterias\SortByCriteria;
use Getxe\Admin\Repositories\ResourceRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Validator;


abstract class ResourceController extends Controller
{
    protected $repo;
    protected $searchable_fields = [];

    public function __construct(ResourceRepository $repo)
    {
        $this->repo = $repo;
    }

    protected function requestValidator() {
        return false;
    }

    protected function validateRequest($request) {
        if($this->requestValidator() !== false) {
            $valid = App::make($this->requestValidator());
            $rules = $valid->rules();
            $messages = $valid->messages();
            Validator::make($request->all(), $rules, $messages)->validate();
        }
    }

    public function getIndexCriterias(Request $request): Collection
    {
        return collect([
            new ExcludeIdsCriteria($request),
            new FilterCriteria($request),
            new SortByCriteria($request)
        ]);
    }
    public function index(Request $request)
    {

        $this->repo->pushCriteria(new SearchByFieldsCriteria($request->input('searchString', ''),$this->searchable_fields));
        $this->getIndexCriterias($request)->each(function($criteria) {
           $this->repo->pushCriteria($criteria);
        });

        $perPage = $request->input('perPage', -1);
        if($perPage > 0) {
            $items = $this->repo->paginate($perPage);
        } else {
            $items = $this->repo->all();
            $items = new LengthAwarePaginator($items, count($items), $perPage);
        }

        return response()->json($items, 200);

//        $query = $this->CurrentModel::excludeIds($input);
//        $filter = $request->input('filter',[]);
//        $query->filter($filter, static::$filterComputedFields);
//        $query->fieldsBySearchString(static::$searchFields,collect($input)->get('searchString', null));
//        $query->sortBy($request->input('sort',null));
//        $result = $this->CurrentModel::filterByProperties($filter, static::$filterComputedFields, $query->get());
//        return response()->json(Pagination::getPagination($result,$input),200);
    }

    public function create()
    {
        return  response()->json($this->repo->makeModel(),200);
    }

    public function store(Request $request)
    {
        $this->validateRequest($request);
        $item = $this->repo->create($request->all());
        $this->repo->attachImages(collect($request->all()), $item);
        return response()->json(['data'=>$item, 'status'=>'ok'],200);
    }


    public function show($id)
    {
        $item = $this->repo->find($id);
        if($item){
            return response()->json($item,200);
        }else{
            return response()->json(['status'=>'Not Found'],404);
        }
    }


    public function edit($id)
    {
        return $this->show($id);
    }


    public function update(Request $request, $id)
    {
        $this->validateRequest($request);
        $updated = $this->repo->update($request->all(), $id);
        $this->repo->attachImages(collect($request->all()), $this->repo->find($id));
        return response()->json([
            'data'=> $this->repo->find($id),
            'updated' => $updated,
            'status'=> ($updated) ? 'ok' : 'error'
        ],200);
    }

    public function destroy($id)
    {
        $this->repo->delete($id);
        return response()->json(['status'=>'ok'],200);
    }
}
