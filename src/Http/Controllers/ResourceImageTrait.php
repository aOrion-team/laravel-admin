<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 23.11.2017
 * Time: 22:30
 */

namespace Getxe\Admin\Http\Controllers;


use Getxe\Admin\Models\Model;
use Illuminate\Support\Collection;

trait ResourceImageTrait
{
    /**
     * @param array $images
     * @param $id
     */
    public function attachImages(Collection $images, Model $item)
    {
        $field_names = [];
        $image_fields = $item->getImageFields();
        foreach ($image_fields as $image_field) {
            if(is_array($image_field) && isset($image_field['name'])) {
                array_push($field_names, $image_field['name']);
            }
            if(is_string($image_field)) {
                array_push($field_names, $image_field);
            }
        }
        $images = $images->only($field_names)->toArray();

        foreach ($images as $field_name => $image) {
            if ($image && count($image) > 0) {
                $this->appendMediaItems($item, $image, $field_name);
            }
        }
    }

    /**
     * @param Collection $images
     * @param bool $single
     * @param array $conversions
     * @return array|null
     */
    protected function prepareImage(Collection $images, $single = true, $conversions = []) {
        if ($single) {
            if($images->count()>0){
                $arImage = [
                    'path'=>$images->first()->getUrl(),
                    'url'=> url($images->first()->getUrl()),
                    'title' => url($images->first()->getCustomProperty('title','')),
                    'alt'   => $images->first()->getCustomProperty('alt'),
                ];
                foreach ($conversions as $conversion) {
                    $arImage[$conversion] = url($images->first()->getUrl($conversion));
                }
                return $arImage;
            }else{
                return null;
            }
        } else {
            if($images->count()>0){
                return $images->map(function($image) use ($conversions){
                    $arImage = [
                        'path'  =>$image->getUrl(),
                        'url'   => url($image->getUrl()),
                        'title' => url($image->getCustomProperty('title','')),
                        'alt'   => $image->getCustomProperty('alt'),
                    ];
                    foreach ($conversions as $conversion) {
                        $arImage[$conversion] = url($image->getUrl($conversion));
                    }
                    return $arImage;
                })->toArray();
            }else{
                return [];
            }
        }

    }

    /**
     * @param $item
     * @param $arMedia
     * @param $mediaCollectionName
     */
    public function appendMediaItems($item,$arMedia,$mediaCollectionName) {

        $imagesForUpdate = collect($arMedia)->map(function($image){
            $image = collect($image);
            return [
                'base_image_path' => $image->get('base_image_path',  $image->get('path',null)),
                'path' => $image->get('path',null),
                'alt'  => $image->get('alt','')
            ];
        })->filter(function($image) {
            if(!!$image['path']) return $image;
        });

        //currently attached items
        $attachedMediaItems = $item->getMedia($mediaCollectionName);

        $ordering = [];

        collect($attachedMediaItems)->each(function($mediaItem) use ($imagesForUpdate, $ordering) {
            $oldUrl = $mediaItem->getUrl();
            $attachedBasePath = $mediaItem->getCustomProperty('base_image_path');

            if (!in_array($oldUrl, $imagesForUpdate->pluck('path')->all(), true)){
                if (in_array($attachedBasePath, $imagesForUpdate->pluck('base_image_path')->all())) {
                    $ordering[$attachedBasePath] = $mediaItem->order_column;
                }
                $mediaItem->delete();
            } else {
                //update alt property for existing image
                $imagesForUpdate->each(function($newImage) use ($mediaItem,$oldUrl){
                    if($newImage['path']===$oldUrl && $newImage['alt'] !== $mediaItem->getCustomProperty('alt')){
                        $mediaItem->setCustomProperty('alt',$newImage['alt']);
                        $mediaItem->save();
                    }
                });
            }
        });

        //items which is not removed
        $attachedMediaItems = collect($attachedMediaItems)->map(function($item){
            return [
                'base_image_path' => $item->getCustomProperty('base_image_path'),
                'path' => $item->getUrl(),
                'alt'  => $item->getCustomProperty('alt'),
            ];
        });

        //find images for update
        $imagesForUpdate = $imagesForUpdate
            ->filter(function($imageItem) use ($attachedMediaItems){
                return !$attachedMediaItems->contains('path',$imageItem['path']);
            });

        foreach ($imagesForUpdate as $i => $imageUpdate){
            $img = $item
                ->addMedia(public_path().$imageUpdate['path'])
                ->preservingOriginal()
                ->withCustomProperties(['alt' => $imageUpdate['alt']])
                ->withCustomProperties(['base_image_path' => $imageUpdate['base_image_path']])
                ->toMediaCollection($mediaCollectionName);
            $img->order_column = $i;
            $img->save();
        }
    }

}