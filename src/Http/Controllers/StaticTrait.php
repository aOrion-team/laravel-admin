<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 23.11.2017
 * Time: 22:30
 */

namespace Getxe\Admin\Http\Controllers;

use Illuminate\Container\Container as App;
use Getxe\Admin\Models\StaticModel;
use Getxe\Admin\Repositories\Configs\YamlConfigRepository;
use Getxe\Admin\Repositories\Settings\StaticModelRepository;

trait StaticTrait
{
    protected $static_model;

    abstract protected function static_model_class();

    protected function makeStaticModel(App $app) {
        $model = $app->make($this->static_model_class());

        if (!$model instanceof StaticModel)
            throw new \Exception("Class {$this->static_model_class()} must be an instance of Getxe\\Admin\\Models\\StaticModel");

        return $this->static_model = $model;
    }

    protected function getStatic(Array $except = [], $resource_name, $config_name) {
        $configRepo = new YamlConfigRepository($resource_name, $config_name);
        $config = $configRepo->get();
        $config_fields = collect($config['fields'])->collapse();
        $config_fields = $config_fields->except($except);
        $settingsRepository = new StaticModelRepository($this->static_model);
        $common_settings = $settingsRepository->get($config_fields->keys());
        return $common_settings;
    }

    protected function saveStatic($data) {
        $settingsRepository = new StaticModelRepository($this->static_model);
        return $settingsRepository->save($data);
    }

}