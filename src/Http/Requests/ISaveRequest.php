<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 13.12.2017
 * Time: 0:02
 */

namespace Getxe\Admin\Http\Requests;


interface ISaveRequest
{
    public function messages();
    public function rules();
}