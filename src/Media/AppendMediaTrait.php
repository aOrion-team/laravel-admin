<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 19.11.2017
 * Time: 15:31
 */

namespace Getxe\Admin\Media;

use Illuminate\Support\Collection;

trait AppendMediaTrait
{

    public function getImageFields() {
        return $this->image_fields;
    }

    public function getAttachments($name, $conversions = []) {
        $arConversions = [];
        foreach ($conversions as $conversion) {
            if (array_key_exists($name,$this->image_fields)
                && is_array($this->image_fields[$name])
                && array_key_exists('conversions',$this->image_fields[$name])
                && is_array($this->image_fields[$name]['conversions'])
                && in_array($conversion, $this->image_fields[$name]['conversions'])
            ) {
                array_push($arConversions, $conversion);
            }
        }
        return $this->prepareImage($this->getMedia($name), $arConversions);
    }

    public function getAttachment($name, $attribute = false, $conversions = []) {
        $media = $this->getAttachments($name, $conversions);
        if(count($media) > 0) {
            $item = $media[0];
            if($attribute != false) {
                return array_key_exists($attribute, $item) ? $item[$attribute] : false;
            }
            return $item;
        } else {
            return false;
        }
    }

    public function mediaAttributesToArray()
    {
        $attributes = [];
        if(isset($this->image_fields)) {
            foreach ($this->image_fields as $image_field) {
                if(is_array($image_field)) {
                    if(isset($image_field['name'])) {
                        if(isset($image_field['conversions']) && is_array($image_field['conversions'])) {
                            $conversions = $image_field['conversions'];
                        } else {
                            $conversions = [];
                        }
                        $attributes[$image_field['name']] = $this->prepareImage($this->getMedia($image_field['name']), $conversions);
                    }
                } else {
                    $attributes[$image_field] = $this->prepareImage($this->getMedia($image_field));
                }

            }
        }
        return $attributes;
    }

    public function prepareImage(Collection $images, $conversions = []) {
        if($images->count()>0){
            return $images->map(function($image) use ($conversions){
                $arImage = [
                    'path'  => $image->getUrl(),
//                    'url'   => substr($image->getUrl(),0, 16).':8000'.substr($image->getUrl(),16),
                    'url'   => url($image->getUrl()),
                    'title' => $image->getCustomProperty('title',''),
                    'alt'   => $image->getCustomProperty('alt'),
                ];
                foreach ($conversions as $conversion) {
                    $arImage[$conversion] = url($image->getUrl($conversion));
                }
                return $arImage;
            })->toArray();
        }else{
            return [];
        }
    }

}