<?php

namespace Getxe\Admin\Pages\Models;

use Illuminate\Database\Eloquent\Model;

abstract class BlocksModel extends Model
{
    protected $fillable = [
        'block_type',
        'sort_index',
        'content'
    ];
    public $timestamps = false;

    public function getContentAttribute($value)
    {
        return json_decode($value);
    }

    abstract public function page();
}
