<?php

namespace Getxe\Admin\Models\Forms;


use Getxe\Admin\Media\AppendMediaTrait;
use Getxe\Admin\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class BaseForm extends Model implements HasMedia
{
    use HasMediaTrait;
    use AppendMediaTrait;
    use SoftDeletes;
    protected $table = 'user_forms';
    protected $fillable = ['name','email','message','page_url','processed'];
}
