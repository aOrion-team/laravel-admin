<?php

namespace Getxe\Admin\Models;

use Carbon\Carbon;
use Getxe\Admin\Media\AppendMediaTrait;
use Illuminate\Database\Eloquent\Model as CoreModel;
use Illuminate\Support\Facades\App;


class Model extends CoreModel
{
    use AppendMediaTrait;
    protected $image_fields = [];
    protected $relation_fields = [];
    protected $relation_fields_id = [];
    protected $slug_source;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->relation_fields_id = collect($this->relation_fields)->map(function($field) { return $field.'_id'; })->toArray();
    }

    public function getFillableRelations() {
        return array_merge($this->getRelationFields(), $this->getRelationFieldsId());
    }

    public function setRelationAttributes($data) {
        $arRelations = collect($data)->only($this->getFillableRelations())->toArray();
        foreach ($arRelations as $key => $value) {
            $this->setAttribute($key,$value);
        }
        return $this;
    }

    public function getRelationIds() {
        $attributes = [];
        foreach ($this->getRelationFields() as $field) {
            if($this->learnMethodType(get_class($this), $field) === 'BelongsTo') {

            } else {
                $attributes[$field.'_id'] = $this->{$field}()
                    ->get(['id'])
                    ->map(function($item) {
                        return (string) $item->id;
                    });
            }

        }
        return $attributes;
    }

    public function attributesToArray()
    {
        return array_merge($this->getRelationIds(), $this->mediaAttributesToArray(), parent::attributesToArray());
    }

    protected function get_short_class($obj){
        return (new \ReflectionClass($obj))->getShortName();
    }
    protected function learnMethodType($classname,$method){

        $obj = new $classname;
        $type = $this->get_short_class($obj->{$method}());
        return $type;
    }

    public function getRelationFields() {
        return $this->relation_fields;
    }

    public function getRelationFieldsId() {
        return $this->relation_fields_id;
    }

//    public function updateRelations($data) {
//        $haveToSave = false;
//        if(count($this->getRelationFields()) > 0) {
//            foreach ($this->getRelationFields() as $relation) {
//                switch ($this->learnMethodType(get_class($this), $relation)) {
//                    case 'BelongsToMany':
//                        $this->{$relation}()->sync($data[$relation.'_id']);
//                        break;
//                    case 'HasMany':
//                        $fk = $this->{$relation}()->getForeignKeyName();
//                        $this->{$relation}()->update([$fk => null]);
//                        $items = App::make(get_class($this->{$relation}()->getRelated()))->whereIn('id', $data[$relation.'_id'])->get();
//                        $this->{$relation}()->saveMany($items);
//                        break;
//                    case 'BelongsTo':
//                        $this->{$relation.'_id'} = $data[$relation]['id'];
//                        $haveToSave = true;
//                        break;
//                }
//            }
//        }
//        if($haveToSave) $this->save();
//    }



    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->getFillableRelations(), true))
        {
            if(in_array($key, $this->getRelationFieldsId(),true)) {
                $relation = substr($key, 0, strlen($key)-3);
            } else if (in_array($key, $this->getRelationFields(),true)) {
                $relation = $key;
            } else {
                $relation = false;
            }

            if($relation !== false) {
                switch ($this->learnMethodType(get_class($this), $relation)) {
                    case 'BelongsToMany':
                        if (in_array($key, $this->getRelationFieldsId(), true)) {
                            $this->{$relation}()->sync($value);
                        }
                        break;
                    case 'HasMany':
                        if (in_array($key, $this->getRelationFieldsId(), true)) {
                            $fk = $this->{$relation}()->getForeignKeyName();
                            if($this->{$relation}()->count() > 0) {
                                $this->{$relation}()->update([$fk => null]);
                            }
                            $items = App::make(get_class($this->{$relation}()->getRelated()))->whereIn('id', $value)
                                ->get();
                            $this->{$relation}()->saveMany($items);
                        }
                        break;
                    case 'BelongsTo':
                        if (in_array($key, $this->getRelationFields(), true)) {
                            $relation_id = (intval($value['id']) > 0) ? intval($value['id']) : null;
                            $this->attributes[$relation.'_id'] = $relation_id;
                        } else {
                            $relation_id = (intval($value) > 0) ? intval($value) : null;
                            return parent::setAttribute($key, $relation_id);
                        }
                        break;
                }
            }
            return $this;
        } else {
            return parent::setAttribute($key, $value);
        }
    }

    public function setSlugAttribute($value){
        if(is_null($value) || $value == ""){
            if(isset($this->slug_source)) {
                $slug = str_slug($this->getAttribute($this->slug_source), "-");
            } else {
                $now = Carbon::now();
                $slug = $now->format('Y-m-d-H-i-s');
            }
        }else{
            $slug = str_slug($value);
        }
        $this->attributes['slug'] = $slug;
    }
}
