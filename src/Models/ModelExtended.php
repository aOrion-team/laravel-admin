<?php

namespace Getxe\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class ModelExtended extends Model
{
    //used in api_url and config path
    protected $resource_name;
    protected $relation_fields = [];
    protected $media_fields = [];
    const fillableFields = [];

    function __construct(array $attributes = [])
    {
        $this->fillable = static::fillableFields;
        parent::__construct($attributes);
    }

    public function getRelationFields()
    {
        return $this->relation_fields;
    }

    public function getGuarded()
    {
        return array_merge($this->guarded,$this->media_fields,$this->relation_fields);
    }
    public function getMediaFields()
    {
        return $this->media_fields;
    }

    /*
     *  Custom Attributes
     */

    public function getResourceNameAttribute()
    {
        if($this->resource_name != ''){
            return $this->resource_name;
        }else {
            return $this->table;
        }
    }
    public function getApiUrlAttribute()
    {
        return '/'.$this->getResourceNameAttribute(). '/';
    }

    /*
     *  END Custom Attributes
     */

    /*
     *  Custom Queries
     */

    /*
     *  Exclude from query items with ids which exist in $arFilter['exclude']
     *
     * @param   $query
     * @return  $arFilter
     */

    public function scopeFilter($query,$filters){
        foreach ($filters as $filter_name => $filter_values){
            if(is_array($filter_values)){
                $query->whereIn($filter_name,$filter_values);
            }else{
                $query->where($filter_name,$filter_values);
            }
        }
    }

    public function scopeSortBy($query,$sort){
        if(isset($sort)) {
            $query->orderBy($sort['field'],$sort['order']);
        } else {
            $query->orderBy('id','asc');
        }
    }


    public function scopeFieldsBySearchString($query, $fields, $searchString )
    {
        if( gettype($fields) == 'string')
        {
            $fields = [$fields];
        }

        $query->where(function ($query) use ($fields, $searchString) {
            foreach ($fields as $field){
                $query->orWhere($field, 'like', '%' . $searchString . '%');
            }
        });

        return $query;
    }

    public function scopeExcludeIds($query, $arExclude,$isInput=true)
    {
        if($isInput){
            if(array_key_exists('exclude',$arExclude)){
                $arExclude = $arExclude['exclude'];
                if(gettype($arExclude) == 'string'){
                    $arExclude = explode(',', $arExclude);
                }
            }else{
                $arExclude = [];
            }
        }else{
            switch (gettype($arExclude)){
                case 'array':
                    break;
                case 'integer':
                    $arExclude = [$arExclude];
                    break;
                case 'string':
                    $arExclude = explode(',', $arExclude);
                    break;
                default:
                    return $query;
                    break;
            }
        }

        return $query->whereNotIn('id', $arExclude);
    }
    /*
     *  END Custom Queries
     */

    static function filterByProperties($filters, $appends, $collection) {
        $customFields = [];
        foreach ($filters as $filter_name => $filter_values) {
            if (in_array($filter_name ,$appends, true)) {
                $customFields[$filter_name] = $filter_values;
            }
        }
        foreach ($customFields as $filter_name => $filter_values){
            if(is_array($filter_values)){
                $collection = $collection->whereIn($filter_name,$filter_values);
            }else{
                $collection = $collection->where($filter_name,$filter_values);
            }
        }
        return $collection;
    }

}
