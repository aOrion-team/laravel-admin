<?php

namespace Getxe\Admin\Models\Pages;

use Getxe\Admin\Pages\Models\BlocksModel;

class SiteBlock extends BlocksModel
{
    protected $table = 'site_blocks';

    public function page()
    {
        return $this->belongsTo('Getxe\Admin\Pages\SitePage', 'page_id');
    }
}
