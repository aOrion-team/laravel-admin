<?php

namespace Getxe\Admin\Models\Pages;

use Getxe\Admin\Models\PagesModel;

class SitePage extends PagesModel
{
    protected $table = 'site_pages';
    protected $guarded = [];

    public function blocks()
    {
        return $this->hasMany('Getxe\Admin\Models\Pages\SiteBlock', 'page_id');
    }
}
