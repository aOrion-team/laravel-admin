<?php

namespace Getxe\Admin\Models;


abstract class PagesModel extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
        'lang',
        'url_name',
        'active',
    ];

    protected $extra_fillable = [
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    public function getFillable()
    {
        return array_merge(parent::getFillable(), $this->extra_fillable);
    }

    public function getPageFields() {
        return $this->extra_fillable;
    }

    abstract public function blocks();
}
