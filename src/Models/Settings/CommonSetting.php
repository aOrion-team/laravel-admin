<?php

namespace Getxe\Admin\Models\Settings;

use Getxe\Admin\Models\StaticModel;

class CommonSetting extends StaticModel
{
    protected $table = 'common_settings';
    protected $key_column_name = 'setting_key';
    protected $value_column_name = 'setting_value';
}
