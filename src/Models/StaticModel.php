<?php

namespace Getxe\Admin\Models;

use Illuminate\Database\Eloquent\Model as CoreModel;

abstract class StaticModel extends CoreModel {
    public $timestamps = false;
    protected $key_column_name = 'setting_key';
    protected $value_column_name = 'setting_value';
    protected $guarded = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fillable = [$this->key_column_name, 'value'];
    }

    /**
     * @return string
     */
    public function getKeyColumnName(): string
    {
        return $this->key_column_name;
    }

    /**
     * @return string
     */
    public function getValueColumnName(): string
    {
        return $this->value_column_name;
    }

    public function setValueAttribute($value) {
        if (is_array($value)) {
            $value = 'text/json:'.json_encode($value);
        }
        $this->attributes[$this->value_column_name] = $value;
    }

    public function getValueAttribute() {
        $value = $this->getAttribute($this->value_column_name);
        if(substr($value,0, 10) === 'text/json:') {
            $value = json_decode(substr($value,10, strlen($value)));
        }
        return $value;
    }
}