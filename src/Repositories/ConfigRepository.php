<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 15:53
 */
namespace Getxe\Admin\Repositories;


abstract class ConfigRepository
{
    protected $resource_name;
    protected $config_name;
    protected $config_path;

    public function __construct(String $resource_name = null, String $config_name = null)
    {
        if($resource_name && $config_name) {
            $this->setup($resource_name, $config_name);
        }
    }
    abstract public function setup(String $resource_name, String $config_name);
    abstract public function get();
}