<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 15:53
 */
namespace Getxe\Admin\Repositories\Configs;

use Getxe\Admin\Repositories\ConfigRepository;
use Symfony\Component\Yaml\Yaml;
use Illuminate\Support\Facades\Cache;

class YamlConfigRepository extends ConfigRepository {

    protected $resource_name;
    protected $config_name;
    protected $config_path;
    protected $config;

    public function setup(String $resource_name, String $config_name) {
        $this->resource_name = $resource_name;
        $this->config_name = $config_name;
        $this->config_path = config_path('admin/' . $this->resource_name . '/' . $this->config_name . '.yml');
        $this->config = $this->read_resource_config();
    }

    public function get()
    {
        return (is_null($this->config)) ? false : $this->config;
    }

    protected function read_resource_config() {
        if(config('app.env') === 'production') {
            return  Cache::remember('config-'.$this->resource_name.'-'.$this->config_name,
                config('cache.remember_time'), function() {
                    return $this->read_resource_config_without_cache();
                });
        } else {
            return $this->read_resource_config_without_cache();
        }
    }

    protected function read_resource_config_without_cache() {
        if (file_exists($this->config_path)) {
            $result = Yaml::parse(file_get_contents($this->config_path));
        } else {
            $result = null;
        }
        return $result;
    }



}