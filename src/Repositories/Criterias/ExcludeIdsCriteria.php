<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 19.11.2017
 * Time: 21:23
 */

namespace Getxe\Admin\Repositories\Criterias;


use Getxe\Admin\Repositories\IResourceRepository;
use Getxe\Admin\Repositories\ResourceCriteria;

class ExcludeIdsCriteria extends ResourceCriteria
{
    protected $exclude;

    public function __construct($request)
    {
        $arExclude = $request->input('exclude');
        switch (gettype($arExclude)){
            case 'array':
                break;
            case 'integer':
                $arExclude = [$arExclude];
                break;
            case 'string':
                $arExclude = explode(',', $arExclude);
                break;
            default:
                $arExclude = false;
                break;
        }
        if ($arExclude) {
            $this->exclude = $arExclude;
        }
    }

    public function apply($query, IResourceRepository $repository)
    {
        if($this->exclude) {
            $query->whereNotIn('id', $this->exclude);
        }
        return $query;
    }
}