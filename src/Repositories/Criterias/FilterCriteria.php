<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 19.11.2017
 * Time: 21:23
 */

namespace Getxe\Admin\Repositories\Criterias;


use Getxe\Admin\Repositories\IResourceRepository;
use Getxe\Admin\Repositories\ResourceCriteria;

class FilterCriteria extends ResourceCriteria
{
    protected $filter;

    public function __construct($request)
    {
        $filter = $request->input('filter',false);
        if ($filter) {
            $this->filter = collect($filter);
        } else {
            $this->filter = collect();
        }
    }

    public function apply($query, IResourceRepository $repository)
    {
        $this->filter->each(function($filter_values, $filter_name) use ($query) {
            if(is_array($filter_values)){
                $query->whereIn($filter_name,$filter_values);
            }else{
                $query->where($filter_name,$filter_values);
            }
        });
        return $query;
    }
}