<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 19.11.2017
 * Time: 21:23
 */

namespace Getxe\Admin\Repositories\Criterias;


use Getxe\Admin\Repositories\IResourceRepository;
use Getxe\Admin\Repositories\ResourceCriteria;

class SearchByFieldsCriteria extends ResourceCriteria
{
    protected $search_fields;
    protected $search_string;

    public function __construct($search_string, $search_fields)
    {
        $this->search_fields = (gettype($search_fields) == 'string') ? [$search_fields] : $search_fields;
        $this->search_string = $search_string;
    }

    public function apply($query, IResourceRepository $repository)
    {
        if($this->search_string) {
            $query->where(function ($query) {
                foreach ($this->search_fields as $field){
                    $query->orWhere($field, 'like', '%' . $this->search_string . '%');
                }
            });
        }
        return $query;
    }
}