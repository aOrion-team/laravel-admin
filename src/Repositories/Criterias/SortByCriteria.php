<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 19.11.2017
 * Time: 21:23
 */

namespace Getxe\Admin\Repositories\Criterias;


use Getxe\Admin\Repositories\IResourceRepository;
use Getxe\Admin\Repositories\ResourceCriteria;

class SortByCriteria extends ResourceCriteria
{
    protected $sort;

    public function __construct($request)
    {
        $sort = $request->input('sort');
        if(is_array($sort) && array_key_exists('field', $sort) && array_key_exists('order', $sort)) {
            $this->sort = $sort;
        }
    }

    public function apply($query, IResourceRepository $repository)
    {
        if($this->sort) {
            $query->orderBy($this->sort['field'], $this->sort['order']);
        }
        return $query;
    }
}