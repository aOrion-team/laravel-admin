<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 16:20
 */

namespace Getxe\Admin\Repositories;


use Illuminate\Support\Collection;

interface IPagesRepository
{
    public function get($url_name, $attribute = 'url_name', $lang = '**');
    public function save($url_name, Collection $pageFields, $attribute = 'url_name', $title = null, $lang = '**');
}