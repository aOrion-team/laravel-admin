<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 23:37
 */

namespace Getxe\Admin\Repositories;


interface IResourceCriteria {

    /**
     * @param bool $status
     * @return $this
     */
    public function skipCriteria($status = true);

    /**
     * @return mixed
     */
    public function getCriteria();

    /**
     * @param ResourceCriteria $criteria
     * @return $this
     */
    public function getByCriteria(ResourceCriteria $criteria);

    /**
     * @param ResourceCriteria $criteria
     * @return $this
     */
    public function pushCriteria(ResourceCriteria $criteria);

    /**
     * @return $this
     */
    public function applyCriteria();
}