<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 23:37
 */

namespace Getxe\Admin\Repositories;

use Getxe\Admin\Models\Model;
use Illuminate\Support\Collection;

interface IResourceImage {
   public function attachImages(Collection $images, Model $item);
}