<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 23:16
 */

namespace Getxe\Admin\Repositories;

interface IResourceRepository {
    public function all($columns = '*');
    public function paginate($perPage = 15, $columns = array('*'));
    public function create(array $data);
    public function update(array $data, $id);
    public function delete($id, $attribute = "id");
    public function find($id, $columns = array('*'));
    public function findBy($field, $value, $columns = array('*'));
}