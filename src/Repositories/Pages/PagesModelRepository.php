<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 16:20
 */

namespace Getxe\Admin\Repositories\Pages;

use Getxe\Admin\Http\Controllers\ResourceImageTrait;
use Getxe\Admin\Models\PagesModel;
use Getxe\Admin\Repositories\IPagesRepository;
use Getxe\Admin\Repositories\IResourceImage;
use Illuminate\Support\Collection;


class PagesModelRepository implements IPagesRepository, IResourceImage
{
    protected $model;
    protected $fieldsCollection;
    protected $withRelations = false;


    use ResourceImageTrait;

    public function withRelations($relations) {
        $this->withRelations = $relations;
    }

    public function __construct(PagesModel $model)
    {
        $this->model = $model;
    }

    public function get($id, $attribute = 'url_name', $lang = '**')
    {
        $page = [];
        $query = $this->model->with('blocks');

        if($this->withRelations === true) {
            $query = $query->with($this->model->getRelationFields());
        } elseif (is_array($this->withRelations)) {
            $query = $query->with($this->withRelations);
        }
        $pageItem = $query->where($attribute, $id)
            ->where('lang', strtoupper($lang))
            ->first();
        if(!$pageItem) {
            foreach ($this->model->getPageFields() as $field) {
                $page[$field.'_'.$lang] = "";
            }
            $page['article_'.$lang] = [];
        } else {
            foreach ($this->model->getPageFields() as $field) {
                $page[$field.'_'.$lang] = $pageItem->{$field};
            }
            $media = $pageItem->mediaAttributesToArray();
            foreach ($this->model->getImageFields() as $field) {
                $page[$field] = $media[$field];
            }
            $page['article_'.$lang] = $pageItem->blocks;
            $page = array_merge($pageItem->getRelationIds(), $page);

            if($this->withRelations === true) {
                foreach ($this->model->getRelationFields() as $field) {
                    $page[$field] = $pageItem->{$field};
                }
            } elseif (is_array($this->withRelations)) {
                foreach ($this->withRelations as $field) {
                    $page[$field] = $pageItem->{$field};
                }
            }
        }
        return $page;
    }

    public function save($id, Collection $pageFields, $attribute = 'url_name', $title = null, $lang = '**')
    {
        $page_keys = collect();
        $pageItem = $this->model->where($attribute, $id)
            ->where('lang', strtoupper($lang))
            ->first();

        $save = false;
        if(!$pageItem) {
            $save = true;
            if($attribute === 'url_name') {
                $url_name = $id;
            } else {
                $url_name = str_slug($title);
            }
            $pageItem = $this->model->fill([
                'title' => ($title) ? $title : 'New Page',
                'lang'                  => strtoupper($lang),
                'url_name'              => $url_name,
                'active'                => true
            ]);
        }

        foreach ($this->model->getPageFields() as $field_name) {
            $page_keys->push($field_name.'_'.$lang);
            $pageItem->setAttribute($field_name, $pageFields->get($field_name.'_'.$lang, ''));
        }

        if ($save) $pageItem->save();

        $relationFields = array_merge($this->model->getRelationFields(), $this->model->getRelationFieldsId());
        foreach ($pageFields as $field_name => $value) {
            if(in_array($field_name, $relationFields)) {
                $page_keys->push($field_name);
                $pageItem->setAttribute($field_name, $value);
            }
        }
        $pageItem->save();

        $pageItem->blocks()->delete();
        $arBlocks = [];
        $article_blocks = $pageFields->get('article_'.$lang, null);
        if(!is_null($article_blocks)) {
            foreach ($article_blocks as $block){
                $arBlocks[] = [
                    'sort_index'=>$block['sort_index'],
                    'block_type' => $block['block_type'],
                    'content'=>json_encode($block['content'])
                ];
            }
        }
        $pageItem->blocks()->createMany($arBlocks);
        $page_keys->push('article_'.$lang);

        $this->attachImages($pageFields, $pageItem);
        $page_keys = $page_keys->merge($pageItem->getImageFields());

        return $page_keys;
    }

    public function create(Collection $pageFields, $title = null, $lang = '**')
    {

        $page_keys = collect();

        $pageItem = $this->model->fill([
            'title' => ($title) ? $title : 'New Page',
            'lang'                  => strtoupper($lang),
            'url_name'              => (string) time(),
            'active'                => true
        ]);

        foreach ($this->model->getPageFields() as $field_name) {
            $page_keys->push($field_name.'_'.$lang);
            $pageItem->setAttribute($field_name, $pageFields->get($field_name.'_'.$lang, ''));
        }
        $pageItem->save();

        $relationFields = array_merge($this->model->getRelationFields(), $this->model->getRelationFieldsId());
        foreach ($pageFields as $field_name => $value) {
            if(in_array($field_name, $relationFields)) {
                $page_keys->push($field_name);
                $pageItem->setAttribute($field_name, $value);
            }
        }
        $pageItem->save();

        $pageItem->blocks()->delete();
        $arBlocks = [];
        foreach ($pageFields->get('article_'.$lang, []) as $block){
            $arBlocks[] = [
                'sort_index'=>$block['sort_index'],
                'block_type' => $block['block_type'],
                'content'=>json_encode($block['content'])
            ];
        }
        $pageItem->blocks()->createMany($arBlocks);
        $page_keys->push('article_'.$lang);

        $this->attachImages($pageFields, $pageItem);
        $page_keys = $page_keys->merge($pageItem->getImageFields());

        return [
            'id' => $pageItem->id,
            'page_keys' => $page_keys
        ];
    }

}