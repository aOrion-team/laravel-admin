<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 16:20
 */

namespace Getxe\Admin\Repositories\Pages;

use Getxe\Admin\Http\Controllers\PageTrait;
use Getxe\Admin\Http\Controllers\StaticTrait;

use Getxe\Admin\Models\Model;
use Getxe\Admin\Models\StaticModel;
use Getxe\Admin\Repositories\Configs\YamlConfigRepository;
use Getxe\Admin\Repositories\ResourceRepository;
use Illuminate\Support\Facades\Cache;


abstract class ResourcePageRepository extends ResourceRepository
{
    use PageTrait;
    use StaticTrait;

    protected $languages = [];
    protected $resource_name;
    protected $config_name = 'detail';

    public function setLanguages($languages) {
        $this->languages = $languages;
    }

    public function setResourceName($resource_name) {
        $this->resource_name = $resource_name;
    }

    public function setConfigName($config_name) {
        $this->config_name = $config_name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Exception
     */
    public function makeModel() {
        $this->makeStaticModel($this->app);
        return $this->model = $this->makePageModel($this->app);
    }


    /**
     * @param $id
     * @param bool $relations
     * @return \Illuminate\Support\Collection|static
     */
    public function find($id, $relations = false){

        $pages = $this->getPages($id, 'id', $relations);
        if($this->resource_name) {
            $pages = $pages->union($this->getStatic($pages->keys()->toArray(), $this->resource_name, $this->config_name)
            )->all();
        }
        return $pages;
    }

    /**
     * @param $attribute
     * @param $value
     * @param bool $relations
     * @return \Illuminate\Support\Collection|static
     */
    public function findBy($attribute, $value, $relations = false) {
        $pages = $this->getPages($value, $attribute, $relations);
        if($this->resource_name) {
            $pages = $pages->union($this->getStatic($pages->keys()->toArray(), $this->resource_name,$this->config_name)
            )->all();
        }
        return $pages;
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        //fetch SitePages and add to $settings
        $created = $this->createPages($data);

        $page_keys = $created['page_keys'];

        $except = $page_keys->all();
        $configRepo = new YamlConfigRepository($this->resource_name, $this->config_name);
        $config = $configRepo->get();
        $data = collect($data)->only(collect($config['fields'])
            ->collapse()
            ->except($except)
            ->keys()
            ->toArray())
            ->toArray();

        $data = $this->saveStatic($data);

        Cache::forget($this->resource_name.'_static');
        return $this->getPages($created['id'], 'id');
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute="id") {

        $page_keys = $this->savePages($id, $data, $attribute);

        $except = $page_keys->all();
        $configRepo = new YamlConfigRepository($this->resource_name, $this->config_name);
        $config = $configRepo->get();
        $data = collect($data)->only(collect($config['fields'])
            ->collapse()
            ->except($except)
            ->keys()
            ->toArray())
            ->toArray();

        $data = $this->saveStatic($data);

        Cache::forget($this->resource_name.'_static');
        return $this->getPages($id, $attribute);
    }
}