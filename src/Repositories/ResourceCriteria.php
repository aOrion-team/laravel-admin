<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 23:35
 */

namespace Getxe\Admin\Repositories;

abstract class ResourceCriteria {

    /**
     * @param $model
     * @param IResourceRepository $repository
     * @return mixed
     */
    public abstract function apply($query, IResourceRepository $repository);
}