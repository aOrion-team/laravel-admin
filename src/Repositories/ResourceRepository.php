<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 16:20
 */

namespace Getxe\Admin\Repositories;


use Carbon\Carbon;
use Getxe\Admin\Http\Controllers\ResourceImageTrait;
use Getxe\Admin\Models\Model;
use Illuminate\Support\Collection;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Log;

abstract class ResourceRepository implements IResourceRepository, IResourceCriteria, IResourceImage
{

    use ResourceImageTrait;

    /**
     * @var App
     */
    protected $app;
    /**
     * @var
     */
    protected $model;

    /**
     * @var
     */
    protected $query;

    /**
     * @var Collection
     */
    protected $criteria;


    /**
     * @var bool
     */
    protected $skipCriteria = false;

    public function __construct(App $app, Collection $collection)
    {
        $this->app = $app;
        $this->criteria = $collection;
        $this->resetScope();
        $this->makeModel();
    }

    /**
     * @return mixed
     */
    abstract public function makeModel();

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    abstract public function update(array $data, $id, $attribute="id");

    /**
     * @param array $data
     * @return mixed
     */
    abstract public function create(array $data);

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*')) {
        $this->applyCriteria();
        return $this->query->get($columns);
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = array('*')) {
        $this->applyCriteria();
        return $this->query->paginate($perPage, $columns);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*')) {
        $this->applyCriteria();
        return $this->query->find($id, $columns);
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*')) {
        $this->applyCriteria();
        return $this->query->where($attribute, '=', $value)->first($columns);
    }

    /**
     * @param $data
     * @return array
     *
     */
    protected function prepareData($data) {
        $arData = collect($data)->only($this->model->getFillable());
        $arDates = collect($data)
            ->only(collect($this->model->getDates())->intersect($this->model->getFillable())->toArray())
            ->map(function($date) {
                return (empty($date)) ? Carbon::now() : $date;
            });
        return $arData->merge($arDates)->toArray();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id, $attribute = "id") {
        return $this->model->whereIn($attribute, explode(',',$id))->delete();
    }



    /**
     * @return $this
     */
    public function resetScope() {
        $this->skipCriteria(false);
        return $this;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function skipCriteria($status = true){
        $this->skipCriteria = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCriteria() {
        return $this->criteria;
    }

    /**
     * @param ResourceCriteria $criteria
     * @return $this
     */
    public function getByCriteria(ResourceCriteria $criteria) {
        $this->model = $criteria->apply($this->model, $this);
        return $this;
    }

    /**
     * @param ResourceCriteria $criteria
     * @return $this
     */
    public function pushCriteria(ResourceCriteria $criteria) {
        $this->criteria->push($criteria);
        return $this;
    }

    /**
     * @return $this
     */
    public function applyCriteria() {

        $this->query = $this->model->newQuery();

        if($this->skipCriteria === true)
            return $this;

        foreach($this->getCriteria() as $criteria) {
            if($criteria instanceof ResourceCriteria)
                $this->query = $criteria->apply($this->query, $this);
        }

        $this->query->with($this->model->getRelationFields());
        return $this;
    }

}