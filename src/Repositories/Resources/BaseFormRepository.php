<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 16:20
 */

namespace Getxe\Admin\Repositories\Resources;

use Getxe\Admin\Models\Forms\BaseForm;

class BaseFormRepository extends ResourceModelRepository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return BaseForm::class;
    }

}