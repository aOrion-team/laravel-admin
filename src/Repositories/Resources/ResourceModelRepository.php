<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 16:20
 */

namespace Getxe\Admin\Repositories\Resources;


use Getxe\Admin\Models\Model;
use Getxe\Admin\Repositories\ResourceRepository;


abstract class ResourceModelRepository extends ResourceRepository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     *
     */
    public abstract function model();

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Exception
     *
     */
    public function makeModel() {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model)
            throw new \Exception("Class {$this->model()} must be an instance of Getxe\\Admin\\Models\\Model");

        return $this->model = $model;
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     *
     */
    public function update(array $data, $id, $attribute="id") {
        $item = $this->model->where($attribute, '=', $id)->with($this->model->getRelationFields())->first();
        $item->fill($this->prepareData($data));
        $item = $item->setRelationAttributes($data);
        $item->save();
        return $data;
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        $item = $this->model->with($this->model->getRelationFields())->create($this->prepareData($data));
        $item = $item->setRelationAttributes($data);
        $item->save();
        return $item;
    }
}