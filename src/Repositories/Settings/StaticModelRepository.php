<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 16:20
 */

namespace Getxe\Admin\Repositories\Settings;

use Getxe\Admin\Models\StaticModel;
use Illuminate\Support\Collection;

use Getxe\Admin\Repositories\StaticRepository;


class StaticModelRepository extends StaticRepository
{
    protected $model;
    protected $fieldsCollection;

    public function __construct(StaticModel $model)
    {
        $this->model = $model;
    }

    public function get(Collection $fields)
    {
        $defaults = $fields->mapWithKeys(function($key) {
            return [$key => ''];
        });

        $result = $this->model->whereIn($this->model->getKeyColumnName(), $fields)
            ->get()
            ->mapWithKeys(function ($item) {
                return [$item[$this->model->getKeyColumnName()] => $item['value']];
            })
            ->union($defaults);

        return $result;
    }

    public function save(Array $settings)
    {
        $saveResult = [
            'created' => 0,
            'updated' => 0
        ];
        foreach ($settings as $setting_key => $setting_value) {
            if($this->model->where($this->model->getKeyColumnName(), $setting_key)->count() > 0) {
                $saveResult['updated']++;
                $update = $this->model->where($this->model->getKeyColumnName(), $setting_key)->first();
                $update->fill(['value' => $setting_value]);
                $update->save();
            } else {
                $saveResult['created']++;
                $this->model->create([
                    $this->model->getKeyColumnName() => $setting_key,
                    'value' => $setting_value
                ]);
            }
        }
        return $saveResult;
    }

}