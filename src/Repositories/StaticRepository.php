<?php
/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 18.11.2017
 * Time: 16:20
 */

namespace Getxe\Admin\Repositories;

use Illuminate\Support\Collection;

abstract class StaticRepository
{
    abstract public function get(Collection $fields);
    abstract public function save(Array $fields);
}