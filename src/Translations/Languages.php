<?php

/**
 * Created by PhpStorm.
 * User: Зверев Валерий
 * Date: 27.11.2017
 * Time: 16:30
 */
namespace Getxe\Admin\Translations;

use Illuminate\Support\Facades\App;

class Languages
{
    static function getCustomPageTranslation($page, $lang = null) {
        if(is_null($lang)) $lang = App::getLocale();
        return collect($page)->filter(function ($value, $key) use ($lang) {
            $locale = substr($key, -2);
            return $locale === $lang || $locale === '**';
        })->mapWithKeys(function ($item, $key) {
            return [substr($key, 0, strlen($key) - 3) => $item];
        });
    }
}