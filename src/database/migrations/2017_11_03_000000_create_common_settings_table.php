<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommonSettingsTable extends Migration {

	public function up()
	{
		Schema::create('common_settings', function(Blueprint $table) {
			$table->increments('id');
            $table->string('setting_key',100);
            $table->text('setting_value')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('common_settings');
	}
}
