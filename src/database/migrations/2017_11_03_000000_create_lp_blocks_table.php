<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLpBlocksTable extends Migration {
    public function up()
    {
        Schema::create('lp_blocks', function(Blueprint $table) {
            $table->increments('id');
            $table->string('block_type',100);
            $table->integer('sort_index')->default(0);
            $table->longText('content');//for json in mysql 5.6
            $table->integer('page_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::drop('lp_blocks');
    }
}
