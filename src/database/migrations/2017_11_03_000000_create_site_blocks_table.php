<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSiteBlocksTable extends Migration {
    public function up()
    {
        Schema::create('site_blocks', function(Blueprint $table) {
            $table->increments('id');
            $table->string('block_type',100);
            $table->integer('sort_index')->default(0);
            $table->longText('content');
            $table->integer('page_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::drop('site_blocks');
    }
}
