<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSitePagesTable extends Migration {
    public function up()
    {
        Schema::create('site_pages', function(Blueprint $table) {
            $table->increments('id');

            $table->enum('lang',['RU', 'EN', '**']);
            $table->string('title', 100);
            $table->string('meta_title', 100);
            $table->string('meta_description', 250);
            $table->string('meta_keywords', 250)->nullable();
            $table->string('url_name',100);
            $table->boolean('active')->default(true);
        });
    }

    public function down()
    {
        Schema::drop('site_pages');
    }
}
