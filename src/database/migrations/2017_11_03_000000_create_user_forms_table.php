<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserFormsTable extends Migration {

    public function up()
    {
        Schema::create('user_forms', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name', 100);
            $table->string('email', 100);
            $table->text('message');
            $table->string('page_url', 1000);
            $table->boolean('processed')->default(false);
        });
    }

    public function down()
    {
        Schema::drop('user_forms');
    }
}
