<?php

use Illuminate\Database\Seeder;

use Getxe\Admin\Models\SitePage;

class SitePagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    static $pages = [
        [
            'title'                  => 'Главная',   // название в админке
            'meta_title'            => '',          // title
            'meta_description'      => '',          // description
            'url_name'              => 'index',     // url
            'active'                => true         // выводить на сайте?
        ]
    ];

    public function run()
    {
        foreach (self::$pages as $page){
            SitePage::create($page);
        }
    }
}
