<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    static $site_domain = 'example.app';

    static $users = [
        [
            'name' => 'Freedots',
            'email' => 'freedots',
            'password' => '123passw0rd123'
        ],
        [
            'name' => 'Администратор',
            'email' => 'admin',
            'password' => '123passw0rd123'
        ]
    ];

    public function run()
    {
        foreach (self::$users as $user){
            User::create([
                'name' => $user['name'],
                'email' => $user['email'].'@'.self::$site_domain,
                'password' => bcrypt($user['password']),
            ]);
        }

    }
}
