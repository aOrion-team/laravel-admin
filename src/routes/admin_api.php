<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['auth.jwt','cors']
],function () {
    //Resources Routes
//    Route::resource('main-page',      'PageController', ['only' => ['index', 'store']]);
    //Test route
    Route::get('test', function () {
        return 'test getxe admin api';
    });
});



