<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * COMMON API ROUTES
 */
Route::group([
    'middleware' => ['cors']
],function () {
    Route::post('auth/signup', 'AuthController@signup');
    Route::post('auth/signin', 'AuthController@signin');
});

Route::group([
    'middleware' => ['cors', 'jwt.refresh']
], function () {
    Route::get('auth/refresh', 'AuthController@refresh');
});

Route::options('{any}', ['middleware' => ['cors'], function () {
    return response(['status' => 'success']);
}])->where('any', '.*');

Route::group([
    'middleware' => ['auth.jwt','cors']
],function () {
    /*  Auth   */
    Route::get('user','AuthController@getCurrentUser');
    Route::post('auth/logout',          'AuthController@logout');
    Route::post('auth/login-other',     'AuthController@loginOther');
    Route::post('auth/change-password', 'AuthController@changePassword');
    Route::post('auth/change-email',    'AuthController@changeEmail');

    /*  Admin App Configs API   */
    Route::get('config/{resource_name}/{config_name}', [
        'uses' => 'ConfigsController@config'
    ]);

    /*  media */
    Route::post('media-items/crop','MediaItemController@crop');
    Route::resource('media-items', 'MediaItemController');
    Route::resource('image-items', 'MediaItemController');
//        Route::resource('media-folders', 'Media\MediaFolderController');
    /*  settings page   */
    Route::get('common-settings',  'SettingsPageController@index');
    Route::post('common-settings', 'SettingsPageController@store');
    /*  dashbord page   */
    Route::get('dashbord','DashbordController@index');
});

Route::group([
    'middleware' => ['auth.jwt','cors']
],function () {
    //Resources Routes
    Route::resource('user-feedbacks', 'BaseFormController');
    Route::resource('main-page',      'PageController', ['only' => ['index', 'store']]);
//    Route::resource('landings', 'LandingController');
});



