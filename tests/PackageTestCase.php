<?php

namespace Vendor\Package\Tests;

use Vendor\Package\AdminServiceProvider;
use Orchestra\Testbench\TestCase;

abstract class PackageTestCase extends TestCase
{

    protected function getPackageProviders($app)
    {
        return [AdminServiceProvider::class];
    }
}
